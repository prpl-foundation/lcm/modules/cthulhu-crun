#!/bin/bash
AMX_CTHULHU_DEFINITION_ODL=/etc/amx/cthulhu/cthulhu_definition.odl
AMX_CTHULHU_DEFAULT_ODL=/etc/amx/cthulhu/cthulhu_defaults.odl
CTHULHU_ODL=/etc/config/cthulhu/odl/cthulhu.odl
LIBCRUN_CONTAINER_C=~/crun/src/libcrun/container.c

if [ $# -eq 0 ]; then
    USE_CRUN=true
    USE_OVERLAY_FS=false
else
    USE_CRUN=$1
    USE_OVERLAY_FS=$2
fi


echo USE_CRUN=$USE_CRUN
echo USE_OVERLAY_FS=$USE_OVERLAY_FS

if [ -f "$AMX_CTHULHU_DEFINITION_ODL" ]; then
    if [[ $USE_OVERLAY_FS == true ]]; then
        sudo sed -i 's#UseOverlayFS = false#UseOverlayFS = true#g' $AMX_CTHULHU_DEFINITION_ODL
    else
        sudo sed -i 's#UseOverlayFS = true#UseOverlayFS = false#g' $AMX_CTHULHU_DEFINITION_ODL
    fi
fi

if [ -f "$AMX_CTHULHU_DEFAULT_ODL" ]; then
    if [[ $USE_CRUN == true ]]; then
        sudo sed -i 's#cthulhu-lxc#cthulhu-crun#g' $AMX_CTHULHU_DEFAULT_ODL
    else
        sudo sed -i 's#cthulhu-crun#cthulhu-lxc#g' $AMX_CTHULHU_DEFAULT_ODL
    fi
fi


if [ -f "$CTHULHU_ODL" ]; then
    if [[ $USE_OVERLAY_FS == true ]]; then
        sudo sed -i 's#UseOverlayFS = false#UseOverlayFS = true#g' $CTHULHU_ODL
    else
        sudo sed -i 's#UseOverlayFS = true#UseOverlayFS = false#g' $CTHULHU_ODL
    fi
    if [[ $USE_CRUN == true ]]; then
        sudo sed -i 's#cthulhu-lxc#cthulhu-crun#g' $CTHULHU_ODL
    else
        sudo sed -i 's#cthulhu-crun#cthulhu-lxc#g' $CTHULHU_ODL
    fi
fi
