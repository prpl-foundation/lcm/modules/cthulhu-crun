# Commands to run in ssh3 to demonstrate container creation via run

# Setup

In ssh session 1:
1. `~/workspace/lcm/modules/cthulhu-crun/demo/crun-demo-setup.sh`
2. `~/workspace/lcm/modules/cthulhu-crun/demo/crun-demo-prpl-startup.sh`

# Subscribing to events

In ssh session 2:
1. `sudo ubus subscribe Cthulhu Rlyeh Timingila | jq .`

# Creating crun container via the Prpl stack

In ssh session 3:
1. `sudo ubus list SoftwareModules*`
2. `sudo crun list`

3. `sudo ubus call SoftwareModules InstallDU '{"URL":"https://index.docker.io/library/hello-world","UUID":"hello-world","ExecutionEnvRef":"generic","AutoStart":"false"}'`
4. `sudo ubus list SoftwareModules*`
5. `sudo ubus call SoftwareModules.ExecutionUnit.1 _get`
6. `sudo crun list`
7. `ls /lcm/cthulhu/rootfs/ -l`
8. `ls /lcm/cthulhu/rootfs/hello-world -l`
9. `ls /lcm/cthulhu/rootfs/hello-world-bundle/ -l`
10. `cat /lcm/cthulhu/rootfs/hello-world-bundle/config.json | more`

## Confirm container can be managed by crun
1. `sudo crun start hello-world`
2. `sudo crun list`
3. `sudo crun delete hello-world`
4. `sudo crun list`

## Uninstall deployment unit
1. `sudo ubus call SoftwareModules.DeploymentUnit.1 Uninstall '{"RetainData":"false"}'`
2. `sudo ubus list SoftwareModules*`

# Creating crun container via crun using same prpl created bundle
1. `cd /lcm/cthulhu/rootfs/hello-world-bundle/`
2. `sudo crun create hello-world`
3. `sudo crun list`
4. `sudo crun start hello-world`
5. `sudo crun delete hello-world`


