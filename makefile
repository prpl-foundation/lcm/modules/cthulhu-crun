include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	-$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(DEST)/usr/lib/$(COMPONENT)/$(COMPONENT).so
	$(INSTALL) -D -p -m 0644 config/default.json $(DEST)/etc/amx/$(COMPONENT)/default.json
#	$(INSTALL) -D -p -m 0644 config/common.seccomp $(DEST)/etc/amx/$(COMPONENT)/common.seccomp
	$(INSTALL) -d -m 0755 $(DEST)/etc/amx/cthulhu/extensions
	$(foreach odl,$(wildcard odl/*.odl), $(INSTALL) -D -p -m 0644 $(odl) $(DEST)/etc/amx/cthulhu/extensions/;)

package: all
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/$(COMPONENT).so $(PKGDIR)/usr/lib/$(COMPONENT)/$(COMPONENT).so
#	$(INSTALL) -D -p -m 0644 config/default.conf $(PKGDIR)/etc/amx/$(COMPONENT)/default.conf
#	$(INSTALL) -D -p -m 0644 config/common.seccomp $(PKGDIR)/etc/amx/$(COMPONENT)/common.seccomp
	$(INSTALL) -d -m 0755 $(PKGDIR)/etc/amx/cthulhu/extensions
	$(INSTALL) -D -p -m 0644 odl/*.odl $(PKGDIR)/etc/amx/cthulhu/extensions/
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	$(MAKE) -C doc doc


test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test
