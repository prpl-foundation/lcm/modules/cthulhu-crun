/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_console_socket.h"

struct socket_fd
{
    int fd;
    struct socket_fd* next;
    char path [MAX_SOCKET_PATH_LENGTH];
};

static struct socket_fd* sockets = NULL;

int create_console_socket (const char* path) {
    int rc = -1;
    struct sockaddr_un sock_addr = {};
    int fd = socket (AF_UNIX, SOCK_STREAM, 0);

    if (fd > 0 && path != NULL) {
        sock_addr.sun_family = AF_UNIX;
        strncpy (sock_addr.sun_path, path, MAX_SOCKET_PATH_LENGTH);

        rc = bind (fd, (struct sockaddr*)&sock_addr, sizeof (struct sockaddr_un));
        if (rc == 0) {
            rc = listen (fd, SOMAXCONN);
            if (rc == 0) {
                struct socket_fd *sock = sockets, *new_socket = NULL;
                new_socket = malloc(sizeof (struct socket_fd));

                while (sock != NULL) { sock = sock->next; }
                
                if (sock != NULL) {
                     sock->next = new_socket; 
                } else {
                    sockets = new_socket;
                }

                new_socket->fd = fd;
                new_socket->next = NULL;
                strncpy (new_socket->path, sock_addr.sun_path, MAX_SOCKET_PATH_LENGTH);
            } else {
                SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to listen on socket '%s', rc = %d", path, errno);
            }
            SAH_TRACEZ_INFO(CTHULHU_CRUN, "Created socket '%s'", path);
        } else {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to bind to socket '%s', rc = %d", path, errno);
        }
    }

    return rc;
}


void close_console_socket(const char* path) {
    struct socket_fd *sock = sockets , *prev = NULL;

    if (path == NULL) { 
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "socket path should not be null");
        return;
    }

    while (sock != NULL) {
        if (strncmp(sock->path,path,MAX_SOCKET_PATH_LENGTH) == 0) {
            close (sock->fd);
            remove(path);

            //remove this socket from the linked list
            if (prev != NULL) { //if not first item in list
                prev->next = sock->next;
            } else if (prev == NULL && sock->next == NULL) { //only 1 item in list so set sockets back to NULL
                sockets = NULL;
            }
            free(sock);
            break;
        }
        prev = sock;
        sock = sock->next;
    }
}

void close_all_console_sockets(void) {
    struct socket_fd *sock = sockets , *tmp = NULL;
    while (sock != NULL) {
        close (sock->fd);
        remove(sock->path);
        tmp = sock;
        sock = sock->next;
        free(tmp);
    }
    sockets = NULL;
}
