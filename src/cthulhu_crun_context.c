/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1

#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <yajl/yajl_gen.h>
#include <pthread.h>
#include <debug/sahtrace.h>

#include <amxc/amxc_variant_type.h>
#include <amxj/amxj_variant.h>

#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_context.h"

static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;

static libcrun_context_t default_ctx = {0};

static const char* log_path;
static const char* log_format;
static const char* DEFAULT_CONFIG_FILE = "/etc/amx/cthulhu-crun/default.json";
static bool enable_open_networking = true;

int init_libcrun_default_context(void) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    int rc = -1;

    int read_length = 0;

    int fd = open(DEFAULT_CONFIG_FILE, O_RDONLY);
    if(fd < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Could not open file [%s] (%d: %s)", DEFAULT_CONFIG_FILE, errno, strerror(errno));
        goto exit;
    }

    amxc_var_t* var = NULL;
    variant_json_t* reader = NULL;

    amxj_reader_new(&reader);

    read_length = amxj_read(reader, fd);
    while(read_length > 0) {
        read_length = amxj_read(reader, fd);
    }

    var = amxj_reader_result(reader);
    amxj_reader_delete(&reader);
    close(fd);

    default_ctx.console_socket = strdup(GET_CHAR(var, "ConsoleSocket"));
    default_ctx.detach= GET_BOOL(var, "Detach");
    default_ctx.preserve_fds = GET_INT32(var, "ConsoleSocket");
    default_ctx.systemd_cgroup = GET_BOOL(var, "SystemdCGroup");
    default_ctx.force_no_cgroup = GET_BOOL(var, "ForceNoCGroup");
    default_ctx.no_subreaper = GET_BOOL(var, "NoSubreaper");
    default_ctx.no_new_keyring = GET_BOOL(var, "NoNewKeyring");
    enable_open_networking = GET_BOOL(var, "EnableOpenNetworking");

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "open networking %s", enable_open_networking ? "enabled": "not enabled");

    default_ctx.notify_socket = getenv("NOTIFY_SOCKET");

    log_path = strdup(GET_CHAR(var, "Log"));
    log_format = strdup(GET_CHAR(var, "LogFormat"));

    amxc_var_delete(&var);

    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static void initialize_additional_settings(amxc_var_t *args, const char* ctr_id) {
    if (GET_ARG(args, "enable_open_networking")) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Open networking for container ID %s : %s", ctr_id, (enable_open_networking == true ? "true" : "false"));
        if (!amxc_var_set(bool, amxc_var_get_key(args, "enable_open_networking", AMXC_VAR_FLAG_DEFAULT), enable_open_networking)) {
            SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to set open networking for container ID %s", ctr_id);
        }
    }
}

int init_libcrun_context(libcrun_context_t* ctx, const char* ctr_id, amxc_var_t* args, libcrun_error_t* crun_err) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    int rc = -1;

    if (ctx == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Null context passed to %s", __func__);
        goto exit;
    }

    // Synchronise initialisation/destruction as libcrun context is not thread safe
    pthread_mutex_lock(&mtx);

    ctx->id = ctr_id;
    ctx->state_root = default_ctx.state_root;
    ctx->console_socket = default_ctx.console_socket;
    ctx->detach = default_ctx.detach;
    ctx->preserve_fds = default_ctx.preserve_fds;
    ctx->systemd_cgroup = default_ctx.systemd_cgroup;
    ctx->force_no_cgroup = default_ctx.force_no_cgroup;
    ctx->no_subreaper = default_ctx.no_subreaper;
    ctx->no_new_keyring = default_ctx.no_new_keyring;
    ctx->notify_socket = default_ctx.notify_socket;

    initialize_additional_settings(args, ctr_id);

    rc = libcrun_init_logging(&ctx->output_handler, &ctx->output_handler_arg, ctr_id, log_path, crun_err);
    if (rc < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to initialise libcrun logging for container '%s'", ctr_id);
        goto unlock;
    }

    if (log_format) {
        rc = libcrun_set_log_format(log_format, crun_err);
        if (rc < 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to set libcrun log format for container '%s'", ctr_id);
            goto unlock;
        }
    }

unlock:
    pthread_mutex_unlock(&mtx);

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

void destroy_libcrun_context(libcrun_context_t* ctx)
{
    if (ctx == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Null context passed to destroy_libcrun_context");
    } else {
        pthread_mutex_lock(&mtx);
        if (ctx->output_handler_arg) {
            fclose(ctx->output_handler_arg);
            ctx->output_handler_arg = NULL;
        }
        pthread_mutex_unlock(&mtx);
    }
}
