/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make strdup work

#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <linux/limits.h>

#include <amxc/amxc_string.h>
#include <amxc/amxc_htable.h>
#include <amxc/amxc_llist.h>
#include <amxc/amxc_variant_type.h>
#include <amxc/amxc_variant.h>

#include <libocispec/image_spec_schema_config_schema.h>
#include <libocispec/runtime_spec_schema_config_schema.h>
#include <debug/sahtrace.h>

#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_oci.h"

static const char passwd_path[] = "/etc/passwd";
static const char group_path[] = "/etc/group";

static const char exposed_ports_annotation[] = "org.opencontainers.image.exposedPorts";
static const char stop_signal_annotation[] = "org.opencontainers.image.stopSignal";
static const char created_annotation[] = "org.opencontainers.image.created";
static const char author_annotation[] = "org.opencontainers.image.author";
static const char architecture_annotation[] = "org.opencontainers.image.architecture";
static const char os_annotation[] = "org.opencontainers.image.os";

static const char volume_mount_type[] = "tmpfs";
static const char volume_mount_source[] = "none";
static const char* volume_mount_options[] = {
    "rw",
    "nosuid",
    "nodev",
    "noexec",
    "relatime"
};

static const char* additional_capabilities[] = {
    "CAP_CHOWN",
    "CAP_SETUID",
    "CAP_SETGID"
};
static const size_t num_additional_capabilities = sizeof(additional_capabilities) / sizeof(char*);

static void free_array(char** arr, size_t arr_len) {
    for (size_t i = 0; i < arr_len; i++) {
        free(arr[i]);
    }
    free(arr);
}

static void* calloc_safe(size_t nitems, size_t size) {
    void *ptr = NULL;
    if (nitems != 0 && size != 0) {
        ptr = calloc(nitems, size);
    }
    return ptr;
}

static int add_additional_capabilities(const char* type, char*** capabilities, size_t* len) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    char** tmp;

    // Ignore unused capability types
    if ((capabilities == NULL) || (len == NULL) || (*len == 0)) {
        rc = 0;
        goto exit;
    }

    size_t new_len = *len;
    tmp = realloc(*capabilities, (new_len + num_additional_capabilities) * sizeof(char*));
    if (tmp == NULL) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to allocate memory for additional %s capabilities", type);
        goto exit;
    }

    for (size_t i = 0; i < num_additional_capabilities; i++) {
        tmp[new_len] = strdup(additional_capabilities[i]);
        ++new_len;
    }

    *capabilities = tmp;
    *len = new_len;

    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_additional_capabilities(runtime_spec_schema_config_schema* config_schema) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    int rc = -1;
    runtime_spec_schema_config_schema_process_capabilities* capabilities = config_schema->process->capabilities;

    when_failed(add_additional_capabilities("bounding", &capabilities->bounding, &capabilities->bounding_len), exit);
    when_failed(add_additional_capabilities("effective", &capabilities->effective, &capabilities->effective_len), exit);
    when_failed(add_additional_capabilities("inheritable", &capabilities->inheritable, &capabilities->inheritable_len), exit);
    when_failed(add_additional_capabilities("permitted", &capabilities->permitted, &capabilities->permitted_len), exit);
    when_failed(add_additional_capabilities("ambient", &capabilities->ambient, &capabilities->ambient_len), exit);

    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int merge_env_list(char ***dest, size_t *dest_cnt, const amxc_llist_t *list) {
    int rc = -1;
    char **res_lst;
    size_t res_idx, res_cnt;

    if ((list == NULL) || (dest == NULL) || (*dest == NULL) || (dest_cnt == NULL)) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid parameters to %s", __func__);
        goto exitrc;
    }

    res_lst = *dest;
    res_cnt = *dest_cnt;
    amxc_llist_iterate(it, (list)) {
        int prefix_sz, env_found;
        const char *entry = amxc_var_get_const_cstring_t(amxc_var_from_llist_it(it));
        const char *eq_ptr = strchr(entry, '=');
        if (eq_ptr == NULL) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid environment entry at %s", __func__);
            goto exit;
        }
        prefix_sz = eq_ptr - entry + 1;
        env_found = 0;
        for (res_idx = 0; res_idx < res_cnt; res_idx++) {
            if (strncmp(res_lst[res_idx], entry, prefix_sz) == 0) {
                env_found = 1;
                break;
            }
        }
        if (env_found) {
            /* update existing entry */
            free (res_lst[res_idx]);
            res_lst[res_idx] = strdup(entry);
        } else {
            /* extend string array */
            char **new_lst = (char**)realloc(res_lst, sizeof(char*) * (res_cnt + 1));
            if (new_lst == NULL) {
                SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Memory alloc failed at %s", __func__);
                goto exit;
            }
            res_lst = new_lst;
            res_lst[res_cnt++] = strdup(entry);
        }
    }
    rc = 0;

exit:
    *dest = res_lst;
    *dest_cnt = res_cnt;
exitrc:
    return rc;
}

static int process_string_list(const amxc_llist_t *list, size_t list_len, char **dest, size_t *dest_len, size_t offset) {
    int rc = -1;

    if ((list == NULL) || (dest == NULL) || (dest_len == NULL)) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid parameters to %s", __func__);
        goto exit;
    }

    *dest_len = offset + list_len;
    amxc_llist_iterate(it, (list)) {
        const char *entry = amxc_var_get_const_cstring_t(amxc_var_from_llist_it(it));
        dest[offset] = strdup(entry);
        offset++;
    }
    rc = 0;

exit:
    return rc;
}

static int get_uid_gid(const char* user, uid_t* uid, gid_t* gid, runtime_spec_schema_config_schema* config_schema) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    FILE* passwd_file = NULL;
    struct passwd* pwd;
    char path[PATH_MAX];

    int chars = snprintf(path, sizeof(path), "%s%s", config_schema->root->path, passwd_path);
    if ((chars < 0) || (chars >= (int)sizeof(path))) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed create path to '%s'", passwd_path);
        goto exit;
    }

    passwd_file = fopen(path, "r");
    if (passwd_file == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to open file '%s'", path);
        goto exit;
    }

    int user_len = (user != NULL) ? strlen(user) : 0;

    setpwent();
    while (true) {
        pwd = fgetpwent (passwd_file);
        if (pwd == NULL) {
            break;
        }
        if (user != NULL) {
            if (strncmp(user, pwd->pw_name, user_len) == 0) {
                *uid = pwd->pw_uid;
                *gid = pwd->pw_gid;
                rc = 0;
                break;
            }
        } else {
            if (*uid == pwd->pw_uid) {
                *gid = pwd->pw_gid;
                rc = 0;
                break;
            }
        }
    }
    endpwent();

exit:
    if (passwd_file != NULL) {
        fclose(passwd_file);
    }
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_additional_gids(const char* user, gid_t user_gid, runtime_spec_schema_config_schema* config_schema) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    FILE* group_file = NULL;
    size_t gid_idx = 0;
    struct group* grp;
    char path[PATH_MAX];
    runtime_spec_schema_config_schema_process_user* user_schema = config_schema->process->user;

    int chars = snprintf(path, sizeof(path), "%s%s", config_schema->root->path, group_path);
    if ((chars < 0) || (chars >= (int)sizeof(path))) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed create path to '%s'", group_path);
        goto exit;
    }

    group_file = fopen(path, "r");
    if (group_file == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to open file '%s'", path);
        goto exit;
    }

    int user_len = (user != NULL) ? strlen(user) : 0;
    setgrent();
    while (true) {
        grp = fgetgrent (group_file);
        if (grp == NULL) {
            rc = 0;
            break;
        }
        if (user_gid == grp->gr_gid) {
            continue;
        }
        for (int i = 0; grp->gr_mem[i] != NULL; i++) {
            if (strncmp(user, grp->gr_mem[i], user_len) == 0) {
                gid_t* p;
                if (gid_idx == 0) {
                    p = malloc(sizeof(gid_t));
                } else {
                    p = realloc(user_schema->additional_gids, sizeof(gid_t) * (gid_idx + 1));
                }
                if (p == NULL) {
                    SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to reallocate additional_gids for user '%s'", user);
                    goto exit;
                }
                user_schema->additional_gids = p;
                user_schema->additional_gids[gid_idx] = grp->gr_gid;
                 ++gid_idx;
             }
         }
         user_schema->additional_gids_len = gid_idx;
         rc = 0;
    }

exit:
    endgrent();
    if (group_file != NULL) {
        fclose(group_file);
    }
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int get_gid_from_group(const char* group, runtime_spec_schema_config_schema* config_schema, gid_t* gid) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    FILE* group_file = NULL;
    struct group* grp;
    char path[PATH_MAX];

    if (snprintf(path, sizeof(path), "%s%s", config_schema->root->path, group_path) < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed create path to '%s'", group_path);
        goto exit;
    }

    group_file = fopen(path, "r");
    if (group_file == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to open file '%s'", path);
        goto exit;
    }

    int group_len = strlen(group);
    setgrent();
    while (true) {
        grp = fgetgrent (group_file);
        if (grp == NULL) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to find group '%s'", group);
            break;
        }
        if (strncmp(group, grp->gr_name, group_len) == 0) {
            *gid = grp->gr_gid;
            rc = 0;
            break;
        }
    }
    endgrent();

exit:
    if (group_file != NULL) {
        fclose(group_file);
    }
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_oci_runtime_process_user(
    const cthulhu_config_t* cthulhu_cfg,
    runtime_spec_schema_config_schema* config_schema,
    bool rootless) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    char* user_cfg = NULL;
    char* user = NULL;
    char* group = NULL;
    runtime_spec_schema_config_schema_process_user* user_schema = config_schema->process->user;

    if ((cthulhu_cfg->user == NULL) || (strlen(cthulhu_cfg->user) == 0)) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "User not defined by image config so using default uid and gid");
        rc = 0;
        goto exit;
    }

    // Get user and optionally group
    user_cfg = strdup(cthulhu_cfg->user);
    char* token = strtok(user_cfg, ":");
    if (token != NULL) {
        user = strdup(user_cfg);
        token = strtok(NULL, ":");
        if (token != NULL) {
            group = strdup(token);
        }
    }

    // There must be a user/uid at least
    if (user == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Image config containers invalid user '%s'", cthulhu_cfg->user);
        goto exit;
    }

    // If uid provided use verbatim, otherwise lookup uid/gid based on user
    errno = 0;
    char *end_ptr;
    uid_t img_uid = strtol(user, &end_ptr, 10);
    uid_t uid = img_uid;
    gid_t gid = 0;
    if (errno != 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid uid in '%s' (%d: %s)", user, errno, strerror(errno));
        goto exit;
    }
    if (end_ptr == user) {
        // Cannot be converted to a uid so obtain uid/gid from user
        if (get_uid_gid(user, &uid, &gid, config_schema) != 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to get uid/gid for user '%s'", user);
            goto exit;
        }
        // Rootless containers require additions gids to be added when user name is provided in the image config
        if (!rootless && (set_additional_gids(user, gid, config_schema) != 0)) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to set additionalGids for user '%s'", user);
            goto exit;
        }
    } else {
        // Confirm that uid exists
        if (get_uid_gid(NULL, &uid, &gid, config_schema) != 0) {
            SAH_TRACEZ_INFO(CTHULHU_CRUN, "Could not find uid '%d', using '%d'", img_uid, uid);
        }
    }

    // If gid passed via user_cfg use verbatim over value obtained from user lookup
    if (group != NULL) {
        errno = 0;
        gid_t img_gid = strtol(group, &end_ptr, 10);
        if (errno != 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid gid in '%s' (%d: %s)", group, errno, strerror(errno));
            goto exit;
        }
        if (end_ptr == group) {
            // Cannot be converted to a gid so obtain gid from group
            if (get_gid_from_group(group, config_schema, &gid) != 0) {
                SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to get gid for group '%s'", group);
                goto exit;
            }
        } else {
            // User group is numeric so use as gid
            gid = img_gid;
        }
    }

    user_schema->uid = uid;
    user_schema->gid = gid;

    rc = 0;
exit:
    free(user_cfg);
    free(user);
    free(group);
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_uid_gid_mappings(runtime_spec_schema_config_schema* config_schema) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    runtime_spec_schema_config_linux* linux_schema = config_schema->linux;
    uid_t uid = config_schema->process->user->uid;

    // Prevent mapping
    errno = 0;
    struct passwd* pw = getpwuid(uid);
    if (pw == NULL) {
        if (errno != 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to get passwd entry for uid '%d' (%d: %s)", uid, errno, strerror(errno));
            goto exit;
        }
        uid = 0;
    }

    // Create mapping from host process uid to container uid
    linux_schema->uid_mappings = malloc(sizeof(runtime_spec_schema_defs_id_mapping*));
    if (linux_schema->uid_mappings == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to allocate memory for uid mappings");
        goto exit;
    }
    linux_schema->uid_mappings[0] = malloc(sizeof(runtime_spec_schema_defs_id_mapping));
    if (linux_schema->uid_mappings == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to allocate memory for uid mapping");
        goto exit;
    }
    linux_schema->uid_mappings[0]->host_id = getuid();
    linux_schema->uid_mappings[0]->host_id_present = 1;
    linux_schema->uid_mappings[0]->container_id = config_schema->process->user->uid;
    linux_schema->uid_mappings[0]->container_id_present = 1;
    linux_schema->uid_mappings[0]->size = 1;
    linux_schema->uid_mappings[0]->size_present = 1;
    linux_schema->uid_mappings[0]->_residual = NULL;
    linux_schema->uid_mappings_len = 1;

    // Create mapping from host process gid to container gid
    linux_schema->gid_mappings = malloc(sizeof(runtime_spec_schema_defs_id_mapping*));
    if (linux_schema->uid_mappings == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to allocate memory for gid mappings");
        goto exit;
    }
    linux_schema->gid_mappings[0] = malloc(sizeof(runtime_spec_schema_defs_id_mapping));
    if (linux_schema->gid_mappings == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to allocate memory for gid mapping");
        goto exit;
    }
    linux_schema->gid_mappings[0]->host_id = getgid();
    linux_schema->gid_mappings[0]->host_id_present = 1;
    linux_schema->gid_mappings[0]->container_id = config_schema->process->user->gid;
    linux_schema->gid_mappings[0]->container_id_present = 1;
    linux_schema->gid_mappings[0]->size = 1;
    linux_schema->gid_mappings[0]->size_present = 1;
    linux_schema->gid_mappings[0]->_residual = NULL;
    linux_schema->gid_mappings_len = 1;

    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_oci_runtime_process(
    const cthulhu_config_t* cthulhu_cfg,
    runtime_spec_schema_config_schema* config_schema,
    bool rootless) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    runtime_spec_schema_config_schema_process* process_schema = config_schema->process;

    process_schema->terminal = true;

    // Set cwd field
    if ((cthulhu_cfg->working_dir != NULL) && (strlen(cthulhu_cfg->working_dir) > 0)) {
        free(process_schema->cwd);
        process_schema->cwd = strdup(cthulhu_cfg->working_dir);
    }

    // Set user field
    when_failed(set_oci_runtime_process_user(cthulhu_cfg, config_schema, rootless), exit);

    // Set uid/gid mappings
    when_failed(set_uid_gid_mappings(config_schema), exit);

    // Merge env field
    when_failed(merge_env_list(&process_schema->env, &process_schema->env_len, &cthulhu_cfg->env), exit);

    // Set args field
    size_t entry_point_len = amxc_llist_size(&(cthulhu_cfg->entry_point));
    size_t cmd_len = amxc_llist_size(&(cthulhu_cfg->cmd));
    size_t new_args_len = entry_point_len + cmd_len;

    free_array(process_schema->args, process_schema->args_len);
    process_schema->args = (char**)calloc_safe(new_args_len, sizeof(char*));
    process_schema->args_len = 0;

    char** args = process_schema->args;
    if (args != NULL) {
        when_failed(process_string_list(&cthulhu_cfg->entry_point, entry_point_len, args, &process_schema->args_len, 0), exit);
        when_failed(process_string_list(&cthulhu_cfg->cmd, cmd_len, args, &process_schema->args_len, entry_point_len), exit);
    }
    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_oci_runtime_annotations(
    const cthulhu_config_t *cthulhu_cfg,
    runtime_spec_schema_config_schema* config_schema) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    const size_t num_labels = amxc_htable_size(&cthulhu_cfg->labels);
    const size_t num_exposed_ports = amxc_llist_size(&cthulhu_cfg->exposed_ports);
    const char* stop_signal = cthulhu_cfg->stop_signal;
    const size_t num_annotations = num_labels +
        ((num_exposed_ports > 0) ? 1 : 0) +
        ((cthulhu_cfg->created != NULL) ? 1 : 0) +
        ((cthulhu_cfg->author != NULL) ? 1 : 0) +
        ((cthulhu_cfg->architecture != NULL) ? 1 : 0) +
        ((cthulhu_cfg->os != NULL) ? 1 : 0) +
        ((stop_signal != NULL) ? 1 : 0);

    int annotation_idx = 0;
    if (num_annotations > 0) {
        json_map_string_string *tmp_annotations = malloc(sizeof(json_map_string_string));
        tmp_annotations->keys = malloc(sizeof(char*) * num_annotations);
        tmp_annotations->values = malloc(sizeof(char*) * num_annotations);
        if (!tmp_annotations || !tmp_annotations->keys || !tmp_annotations->values) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "No memory available for annotations");
            free(tmp_annotations->keys);
            free(tmp_annotations->values);
            free(tmp_annotations);
            return -ENOMEM;
        }
        // Process exposed_ports
        if (num_exposed_ports > 0) {
            amxc_string_t exposed_ports;
            amxc_string_init(&exposed_ports, 0);
            int i = 0;
            amxc_llist_iterate(it, (&cthulhu_cfg->exposed_ports)) {
                const char *entry = amxc_var_get_const_cstring_t(amxc_var_from_llist_it(it));
                const char *del = i == 0 ? "" : ",";
                amxc_string_appendf(&exposed_ports, "%s%s", del, entry);
                ++i;
            }
            tmp_annotations->keys[annotation_idx] = strdup(exposed_ports_annotation);
            tmp_annotations->values[annotation_idx] = strdup(amxc_string_get(&exposed_ports, 0));
            amxc_string_clean(&exposed_ports);
            ++annotation_idx;
        }

        if (cthulhu_cfg->created != NULL) {
            tmp_annotations->keys[annotation_idx] = strdup(created_annotation);
            tmp_annotations->values[annotation_idx] = strdup(cthulhu_cfg->created);
            annotation_idx++;
        }

        if (cthulhu_cfg->author != NULL) {
            tmp_annotations->keys[annotation_idx] = strdup(author_annotation);
            tmp_annotations->values[annotation_idx] = strdup(cthulhu_cfg->author);
            annotation_idx++;
        }

        if (cthulhu_cfg->architecture != NULL) {
            tmp_annotations->keys[annotation_idx] = strdup(architecture_annotation);
            tmp_annotations->values[annotation_idx] = strdup(cthulhu_cfg->architecture);
            annotation_idx++;
        }

        if (cthulhu_cfg->os != NULL) {
            tmp_annotations->keys[annotation_idx] = strdup(os_annotation);
            tmp_annotations->values[annotation_idx] = strdup(cthulhu_cfg->os);
            annotation_idx++;
        }

        // Process stop signal
        if (stop_signal != NULL) {
            tmp_annotations->keys[annotation_idx] = strdup(stop_signal_annotation);
            tmp_annotations->values[annotation_idx] = strdup(stop_signal);
            annotation_idx++;
        }

        // Process labels
        if (num_labels > 0) {
            amxc_htable_iterate(it, &(cthulhu_cfg->labels)) {
                const char *key = amxc_htable_it_get_key(it);
                amxc_var_t *value = amxc_var_from_htable_it(it);
                tmp_annotations->keys[annotation_idx] = strdup(key);
                tmp_annotations->values[annotation_idx] = strdup(amxc_var_get_const_cstring_t(value));
                ++annotation_idx;
            }
        }
        free_json_map_string_string(config_schema->annotations);
        config_schema->annotations = tmp_annotations;
        config_schema->annotations->len = num_annotations;
    }

    // Further annotations to be implemented once cthulhu_config_t has been extended

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned 0", __func__);
    return 0;
}

static int set_oci_runtime_volume_mounts(
    const cthulhu_config_t *cthulhu_cfg,
    runtime_spec_schema_config_schema* config_schema) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = 1;
    size_t num_volumes = amxc_llist_size(&cthulhu_cfg->volumes);
    if (num_volumes > 0) {
        size_t num_existing_mounts = config_schema->mounts_len;

        runtime_spec_schema_defs_mount** mounts =
            realloc(config_schema->mounts, sizeof(runtime_spec_schema_defs_mount*) * (num_existing_mounts + num_volumes));

        if (mounts == NULL) {
            SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to allocate memory for volume mounts");
            goto exit;
        }

        int i = num_existing_mounts;
        amxc_llist_iterate(it, (&cthulhu_cfg->volumes)) {
            const char *entry = amxc_var_get_const_cstring_t(amxc_var_from_llist_it(it));
            mounts[i] = calloc(1, sizeof(runtime_spec_schema_defs_mount));
            mounts[i]->source = (char*)volume_mount_source;
            mounts[i]->destination = strdup(entry);
            mounts[i]->type = (char*)volume_mount_type;
            mounts[i]->options = (char**)volume_mount_options;
            mounts[i]->options_len = sizeof(volume_mount_options) / sizeof(char*);
            i++;
        }
        config_schema->mounts = mounts;
        config_schema->mounts_len = num_existing_mounts + num_volumes;
    }
    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static int set_oci_runtime_linux_namespaces(
    runtime_spec_schema_config_schema* config_schema) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    runtime_spec_schema_defs_linux_namespace_reference** linux_ns = config_schema->linux->namespaces;
    const size_t namespace_count = config_schema->linux->namespaces_len;
    const size_t namespace_size = sizeof(runtime_spec_schema_defs_linux_namespace_reference);
    const char* discard_namespaces = "network";

    size_t new_ns_idx = 0;
    size_t discarded_ns_cnt = 0;
    runtime_spec_schema_defs_linux_namespace_reference* tmp_arr[namespace_size];

    for (size_t idx = 0; idx < namespace_count; ++idx ) {
        if(strncmp(linux_ns[idx]->type, discard_namespaces, (sizeof(discard_namespaces) / sizeof(char))) == 0) {
            free_runtime_spec_schema_defs_linux_namespace_reference(linux_ns[idx]);
            discarded_ns_cnt++;
            continue;
        }
        tmp_arr[new_ns_idx] = linux_ns[idx];
        new_ns_idx++;
    }
    free(linux_ns);

    const size_t new_ns_count = new_ns_idx;
    runtime_spec_schema_defs_linux_namespace_reference **linux_tmp_ns =
        calloc(new_ns_count, sizeof(runtime_spec_schema_defs_linux_namespace_reference *));

    if (!linux_tmp_ns) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to allocate memory for volume mounts");
        return errno;
    }

    for (size_t idx = 0; idx < new_ns_count; ++idx) {
        linux_tmp_ns[idx] = tmp_arr[idx];
    }

    config_schema->linux->namespaces = linux_tmp_ns;
    config_schema->linux->namespaces_len = new_ns_count;
    return 0;
}

int create_oci_runtime_config(
    const cthulhu_config_t* cthulhu_cfg,
    const char* bundle_rootfs,
    const char* bundle_cfg,
    bool rootless,
    bool enable_open_networking) {

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    FILE* fp = NULL;
    int rc = -1;
    parser_error parse_err;

    if ((cthulhu_cfg == NULL) || (bundle_rootfs == NULL) || (bundle_cfg == NULL))
    {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid parameters to %s", __func__);
        goto exit;
    }

    // Parse the bundle config into a runtime schema structure
    runtime_spec_schema_config_schema *config_schema =
        runtime_spec_schema_config_schema_parse_file(bundle_cfg, NULL, &parse_err);

    if (config_schema == NULL)
    {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to parse runtime config schema '%s'", &parse_err);
        goto exit;
    }

    // Mutate the runtime config based on the image spec data held in the cthulhu_config_t structure
    // do this only if bundle is not provided and we deal with images. Enable network anyway.
    config_schema->root->path = strdup(bundle_rootfs);
    config_schema->root->readonly = false;
    if (!cthulhu_cfg->bundle_provided) {
        when_failed(set_oci_runtime_process(cthulhu_cfg, config_schema, rootless), exit);
        when_failed(set_oci_runtime_annotations(cthulhu_cfg, config_schema), exit);
        when_failed(set_oci_runtime_volume_mounts(cthulhu_cfg, config_schema), exit);
        when_failed(set_additional_capabilities(config_schema), exit);
    }
    if (enable_open_networking) {
        when_failed(set_oci_runtime_linux_namespaces(config_schema), exit);
    }

    // Generate json for the mutated runtime config
    char* json_buf = runtime_spec_schema_config_schema_generate_json(config_schema, NULL, &parse_err);
    if (json_buf == NULL) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to parse default runtime config schema '%s'", &parse_err);
        goto exit;
    }

    // Open the existing bundle config file for writing
    fp = fopen(bundle_cfg, "w");
    if (fp == NULL) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to open file '%s'", bundle_cfg);
        goto exit;
    }

    // Write json and check bytes written is correct
    size_t len = strlen(json_buf);
    if (fwrite(json_buf, sizeof(char), len, fp) != len) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Failed to write file '%s'", bundle_cfg);
        goto exit;
    }

    rc = 0;

exit:
    if (fp != NULL) {
        fclose(fp);
    }
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}
