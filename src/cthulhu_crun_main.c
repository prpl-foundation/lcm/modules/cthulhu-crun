/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1

#include <stdlib.h>
#include <fcntl.h>
#include <amxc/amxc_variant.h>
#include <amxm/amxm.h>
#include <cthulhu/cthulhu_defines.h>
#include <debug/sahtrace.h>
#include <string.h>

#include "cthulhu_crun.h"
#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_create.h"
#include "cthulhu_crun_start.h"
#include "cthulhu_crun_stop.h"
#include "cthulhu_crun_remove.h"
#include "cthulhu_crun_list.h"
#include "cthulhu_crun_version.h"
#include "cthulhu_crun_console_socket.h"
#include "cthulhu_crun_context.h"
#include "cthulhu_crun_information.h"

#define MOD_CTHULHU "mod_cthulhu"

static amxm_shared_object_t* so = NULL;
static amxm_module_t* mod = NULL;

// the file descriptor of the master pseudo terminal
static int fd_ptm = -1;
// the name of the slave side of the pseudo terminal
static char* pts_name = NULL;

static int cthulhu_pts_setup(void) {
    int ret = -1;
    char* slavename = NULL;
    if((fd_ptm = open("/dev/ptmx", O_RDWR)) < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Could not open /dev/ptmx");
        goto exit;
    }
    grantpt(fd_ptm);
    unlockpt(fd_ptm);
    slavename = ptsname(fd_ptm);
    if(!slavename) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Could not get the node name of the pts");
        goto exit;
    }
    pts_name = strdup(slavename);
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "Use pts %s", pts_name);

exit:
    return ret;
}

static void cthulhu_pts_cleanup(void) {
    if(fd_ptm < 0) {
        close(fd_ptm);
        fd_ptm = -1;
    }
    free(pts_name);
    pts_name = NULL;
}

AMXM_CONSTRUCTOR module_init(void) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = 1;

    so = amxm_so_get_current();
    if(so == NULL) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Could not get shared object\n");
        goto exit;
    }

    if(amxm_module_register(&mod, so, MOD_CTHULHU)) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Could not register module %s\n", MOD_CTHULHU);
        goto exit;
    }
    cthulhu_pts_setup();

    when_failed(init_libcrun_default_context(), exit);
    when_failed(cthulhu_crun_stop_init(), exit);

    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_CREATE, cthulhu_crun_create), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_START, cthulhu_crun_start), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_STOP, cthulhu_crun_stop), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_REMOVE, cthulhu_crun_remove), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_LIST, cthulhu_crun_list), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_VERSION, cthulhu_crun_version), exit);
    when_failed(amxm_module_add_function(mod, CTHULHU_BACKEND_CMD_INFORMATION, cthulhu_crun_information), exit);

    rc = 0;

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

AMXM_DESTRUCTOR module_exit(void) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    close_all_console_sockets();
    amxm_module_deregister(&mod);
    cthulhu_pts_cleanup();
    cthulhu_crun_stop_destroy();
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned 0", __func__);
    return 0;
}
