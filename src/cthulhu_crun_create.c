/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make symlink work

#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <yajl/yajl_gen.h>
#include <ctype.h>
#include <stdlib.h>
#include <linux/limits.h>

#include <amxc/amxc_variant_type.h>
#include <amxc/amxc_variant.h>
#include <amxj/amxj_variant.h>

#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include <crun/container.h>
#include <debug/sahtrace.h>
#include <crun/libocispec/json_common.h>

#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_context.h"
#include "cthulhu_crun_console_socket.h"
#include "cthulhu_crun_oci.h"
#include "cthulhu_crun_create.h"

static const char rootless_mode_env_var[] = "CTHULHU_CRUN_ROOTLESS_MODE";
static const char bundles_dir_name[] = "bundles";
static const char bundle_rootfs_dir_name[] = "rootfs";
static const char bundle_config_filename[] = "config.json";

static int write_config(const char* file, amxc_var_t* var) {
    int write_length = 0;
    variant_json_t* writer = NULL;
    int fd = 0;

    fd = open(file, O_WRONLY | O_CREAT, 0644);
    if(fd < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Could not create file [%s] (%d: %s)", file, errno, strerror(errno));
        goto exit;
    }

    amxj_writer_new(&writer, var);
    when_null(writer, exit);

    write_length = amxj_write(writer, fd);

exit:
    if(fd) {
        close(fd);
    }
    amxj_writer_delete(&writer);
    return write_length;
}

static int create_bundle_config(const char* bundle_path, bool rootless, bool use_bundles, libcrun_error_t* crun_err) {
    int rc = -1;
    FILE* bundle_config_file = NULL;
    char bundle_config_path[PATH_MAX + sizeof(bundle_config_filename)];

    if ((bundle_path == NULL) || (crun_err == NULL)) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid parameters to %s", __func__);
        goto exit;
    }

    if (chdir(bundle_path) < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to change to bundle path '%s'", bundle_path);
        goto exit;
    }

    if (snprintf(bundle_config_path, sizeof(bundle_config_path), "%s/%s", bundle_path, bundle_config_filename) < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Bundle config path truncated for bundle '%s/%s'", bundle_path, bundle_config_filename);
        goto exit;
    }

    if (access(bundle_config_path, F_OK) == 0) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Default bundle config path already exists '%s'", bundle_config_path);
    } else {
        if (use_bundles) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Provided bundle does not contain config '%s'", bundle_config_path);
            goto exit;
        }

        bundle_config_file = fopen(bundle_config_path, "w+");
        if (bundle_config_file == NULL) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to create file '%s'", bundle_config_path);
            goto exit;
        }

        int local_ret_val = libcrun_container_spec(!rootless, bundle_config_file, crun_err);
        if (local_ret_val < 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to create default bundle config '%s', rc = %d", bundle_config_path, local_ret_val);
            goto exit;
        }
    }

    rc = 0;

exit:
    if (bundle_config_file != NULL) {
        if (fclose(bundle_config_file) != 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to close file '%s'", bundle_config_path);
            rc = -1;
        }
    }
    return rc;
}

static int create_crun_container(
    const cthulhu_config_t* cthulhu_cfg,
    libcrun_context_t* crun_ctx,
    libcrun_error_t* crun_err) {

    int rc = -1;

    if ((cthulhu_cfg == NULL) || (crun_ctx == NULL) || (crun_err == NULL)) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid parameters to %s", __func__);
        goto exit;
    }

    libcrun_container_t* container = libcrun_container_load_from_file (bundle_config_filename, crun_err);
    if (container == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to load container '%s'", cthulhu_cfg->id);
        goto exit;
    }

    if (create_console_socket(crun_ctx->console_socket) < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to create console socket for container '%s'", cthulhu_cfg->id);
        goto exit;
    }

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "Successfully loaded bundle config for container '%s'", cthulhu_cfg->id);

    crun_ctx->bundle = getcwd(NULL, 0);
    if (getenv ("LISTEN_FDS"))
    {
        crun_ctx->listen_fds = strtoll (getenv ("LISTEN_FDS"), NULL, 10);
        crun_ctx->preserve_fds += crun_ctx->listen_fds;
    }

    if (libcrun_container_create (crun_ctx, container, LIBCRUN_RUN_OPTIONS_PREFORK, crun_err) != 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "CTHULHU_CRUN, Failed to create container '%s'", cthulhu_cfg->id);
        goto exit;
    }

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "Successfully created container '%s'", cthulhu_cfg->id);
    rc = 0;

exit:
    return rc;
}

int cthulhu_crun_create(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    bool rootless = true;
    libcrun_context_t crun_ctx = {0};
    libcrun_error_t crun_err;
    amxc_string_t cthulhu_data_filename;
    amxc_string_t time_string;
    amxc_var_t cthulhu_data;
    char bundle_path[PATH_MAX];
    char bundle_rootfs_path[PATH_MAX + sizeof(bundle_rootfs_dir_name)];

    when_failed(amxc_string_init(&cthulhu_data_filename, 0), exit);
    when_failed(amxc_string_init(&time_string, 0), exit);
    amxc_var_init(&cthulhu_data);

    const cthulhu_config_t* cthulhu_cfg = amxc_var_get_const_cthulhu_config_t(args);
    if (cthulhu_cfg == NULL) {
        SET_ERR("Failed to get cthulhu config");
        goto exit;
    }

    if (string_is_empty(cthulhu_cfg->id) || string_is_empty(cthulhu_cfg->rootfs)) {
        SET_ERR("Invalid cthulhu config");
        goto exit;
    }

    amxc_var_t ns;
    amxc_var_init(&ns);
    amxc_var_set_type(&ns, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(bool, &ns, "enable_open_networking", false);

    int local_ret_val = init_libcrun_context(&crun_ctx, cthulhu_cfg->id, &ns, &crun_err);
    if (local_ret_val < 0) {
        SET_ERR("Failed to initialise libcrun context for container '%s', rc = %d", cthulhu_cfg->id, local_ret_val);
        goto exit;
    }

    const bool enable_open_networking = GET_BOOL(&ns, "enable_open_networking");
    amxc_var_clean(&ns);

    time_t now;
    time(&now);

    amxc_string_setf(&time_string, "%s", ctime(&now));  // strip '\n'
    amxc_string_trimr(&time_string, iscntrl);

    if (snprintf(bundle_path, sizeof(bundle_path), "%s/../../%s/%s",  cthulhu_cfg->rootfs, bundles_dir_name, cthulhu_cfg->id) < 0) {
        SET_ERR("Bundle path truncated for container '%s'", cthulhu_cfg->id);
        goto exit;
    }

    if (cthulhu_mkdir(bundle_path, true) != 0) {
        SET_ERR("Failed to create bundle directory '%s'", bundle_path);
        goto exit;
    }

    if (snprintf(bundle_rootfs_path, sizeof(bundle_rootfs_path), "%s/%s", bundle_path, bundle_rootfs_dir_name) < 0) {
        SET_ERR("Bundle rootfs path truncated for container '%s'", cthulhu_cfg->id);
        goto exit;
    }

    if (access(bundle_rootfs_path, F_OK) == 0) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Bundle rootfs path already exists '%s'", bundle_rootfs_path);
    } else {
        local_ret_val = symlink(cthulhu_cfg->rootfs, bundle_rootfs_path);
        if ((local_ret_val != 0) && (errno != EEXIST)) {
            SET_ERR("Failed to create bundle symlimk from '%s' to '%s'", bundle_rootfs_path, cthulhu_cfg->rootfs);
            goto exit;
        }
    }

    char* rootless_env = getenv(rootless_mode_env_var);
    if (rootless_env != NULL) {
        errno = 0;
        char *end_ptr;
        int env_val = strtol(rootless_env, &end_ptr, 10);
        // Rootless if error or non-numeric or value non-zero
        rootless = ((errno != 0) || (end_ptr == rootless_env) || (env_val != 0));
        if (!rootless) {
            SAH_TRACEZ_INFO(CTHULHU_CRUN, "Creating privileged container '%s'", cthulhu_cfg->id);
        }
    }

    rc = create_bundle_config(bundle_path, rootless, cthulhu_cfg->bundle_provided, &crun_err);
    if (rc < 0) {
        SET_ERR("Failed to create default bundle config '%s'", bundle_path);
        goto exit;
    }

    rc = create_oci_runtime_config(cthulhu_cfg, bundle_rootfs_dir_name, bundle_config_filename, rootless,
                                   enable_open_networking);
    if (rc < 0) {
        SET_ERR("Failed to create bundle config '%s'", bundle_path);
        goto exit;
    }

    rc = create_crun_container(cthulhu_cfg, &crun_ctx, &crun_err);
    if (rc < 0) {
        SET_ERR("Failed to create crun container '%s'", bundle_path);
        goto exit;
    }

    // save the bundle name
    amxc_var_set_type(&cthulhu_data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "bundle_name", cthulhu_cfg->bundle_name);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "bundle_version", cthulhu_cfg->bundle_version);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "sb_id", cthulhu_cfg->sandbox);
    amxc_var_add_new_key_cstring_t(&cthulhu_data, "create_time", time_string.buffer);
    amxc_string_setf(&cthulhu_data_filename, "%s/cthulhu.json", bundle_path);
    write_config(cthulhu_data_filename.buffer, &cthulhu_data);

exit:
    destroy_libcrun_context(&crun_ctx);
    amxc_var_set(bool, ret, rc);
    amxc_var_clean(&cthulhu_data);
    amxc_string_clean(&cthulhu_data_filename);
    amxc_string_clean(&time_string);
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}
