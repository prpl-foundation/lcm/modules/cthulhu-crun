include ../makefile.inc

# build destination directories
OBJDIR = ../output/$(MACHINE)

# TARGETS
TARGET_SO = $(OBJDIR)/$(COMPONENT).so

# directories
# source directories
INCDIRS = $(INCDIR) $(INCDIR_PRIV)
ifeq ($(CONFIG_SAH_LIB_CTHULHU-CRUN_PRPL_OS_RPATH),y)
	STAGINGLIBDIR = -L$(STAGINGDIR)/opt/prplos/usr/lib
	INCDIRS += $(if $(STAGINGDIR), $(STAGINGDIR)/opt/prplos/usr/include)
else
	STAGINGLIBDIR =
endif

SRCDIR = $(shell pwd)
INCDIR_PRIV = ../include_priv
INCDIRS += $(INCDIR_PRIV) $(if $(STAGINGDIR), $(STAGINGDIR)/include)
INCDIRS += $(if $(STAGINGDIR), $(STAGINGDIR)/usr/include/crun $(STAGINGDIR)/usr/include/crun/libocispec, /usr/include/crun /usr/include/crun/libocispec)
STAGINGLIBDIR += $(if $(STAGINGDIR), -L$(STAGINGDIR)/lib -L$(STAGINGDIR)/usr/lib)

# files
SOURCES = $(wildcard $(SRCDIR)/*.c)
OBJECTS = $(addprefix $(OBJDIR)/,$(notdir $(SOURCES:.c=.o)))

# compilation and linking flags
CFLAGS +=  -Werror -Wall -Wextra \
		-Wformat=2 -Wshadow \
		-Wwrite-strings -Wredundant-decls \
		-Wmissing-declarations -Wno-attributes \
		-Wno-format-nonliteral \
		-DSAHTRACES_ENABLED -DSAHTRACES_LEVEL=500 \
		-fPIC -g3 $(addprefix -I ,$(INCDIRS))

ifeq ($(CC_NAME),g++)
	CFLAGS += -std=c++2a
else
	CFLAGS += -Wstrict-prototypes -Wold-style-definition -Wnested-externs -std=c11
endif

LDFLAGS += $(STAGINGLIBDIR) \
		-shared -fPIC \
		-lamxc -lamxm \
		-lamxj -lyajl \
		-lamxp -lsahtrace \
		-lcrun -lcthulhu \
		-locispec

# targets
all: $(TARGET_SO)

$(TARGET_SO): $(OBJECTS)
ifeq ($(CONFIG_SAH_LIB_CTHULHU-CRUN_PRPL_OS_RPATH),y)
	echo "!!! Compiling with --rpath to /opt/prplos/usr/lib/ !!!"
	$(CC) -Wl,-rpath=/opt/prplos/usr/lib/,-soname,$(COMPONENT).so -o $@ $(OBJECTS) $(LDFLAGS)
else
	$(CC) -Wl,-soname,$(COMPONENT).so -o $@ $(OBJECTS) $(LDFLAGS)
endif

-include $(OBJECTS:.o=.d)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	$(MKDIR) -p $@

clean:
	rm -rf ../output/

.PHONY: clean
