/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make strdup work

#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <yajl/yajl_gen.h>
#include <linux/limits.h>

#include <amxc/amxc_variant_type.h>
#include <amxc/amxc_variant.h>
#include <amxc/amxc_llist.h>
#include <amxj/amxj_variant.h>
#include <cthulhu/cthulhu.h>
#include <cthulhu/cthulhu_defines.h>
#include <crun/status.h>
#include <debug/sahtrace.h>

#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_context.h"
#include "cthulhu_crun_list.h"

typedef struct {
    const char* crun_status;
    cthulhu_ctr_status_t cthulhu_status;
} crun_cthulhu_status_map_t;

static crun_cthulhu_status_map_t crun_cthulhu_status_map[] = {
//    crun status   cthulhu status
    { "created",    cthulhu_ctr_status_stopped },
    { "stopped",    cthulhu_ctr_status_stopped },
    { "running",    cthulhu_ctr_status_running },
    { "paused",     cthulhu_ctr_status_frozen  }
};

#define CRUN_CTHULHU_STATUS_MAP_LENGTH (sizeof(crun_cthulhu_status_map) / sizeof(crun_cthulhu_status_map[0]))

static amxc_var_t* read_json(const char* file) {
    variant_json_t* reader = NULL;
    amxc_var_t* json_data = NULL;
    size_t read_length = 0;
    int json_fd = open(file, O_RDONLY);

    if(json_fd < 0) {
        SAH_TRACEZ_INFO(CTHULHU_CRUN, "Could not open %s errno:%d)", file, errno);
    } else {
        amxj_reader_new(&reader);

        if (reader != NULL) {
            read_length = amxj_read(reader, json_fd);
            while(read_length > 0) {
                read_length = amxj_read(reader, json_fd);
            }
        }

        json_data = amxj_reader_result(reader);
        
        if(json_fd != 0) {
            close(json_fd);
        }
        amxj_reader_delete(&reader);
    }

    return json_data;
}

static cthulhu_ctr_status_t get_cthulhu_ctr_status(const char* crun_ctr_status) {    
    cthulhu_ctr_status_t cthulhu_ctr_status = cthulhu_ctr_status_unknown;
    if (crun_ctr_status != NULL) {
        for (size_t i = 0 ; i < CRUN_CTHULHU_STATUS_MAP_LENGTH; i++) {
            if (strcmp(crun_ctr_status, crun_cthulhu_status_map[i].crun_status) == 0) {
                cthulhu_ctr_status = crun_cthulhu_status_map[i].cthulhu_status;
                break;
            }
        }
    }
    return cthulhu_ctr_status;
}

static int set_bundle_version(const char* ctr_id, cthulhu_ctr_info_t* ctr_info) {
    //get bundle version
    int rc = -1;

    if ((ctr_id == NULL) || (ctr_info == NULL)) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Invalid parameters to %s", __func__);
        goto exit;
    }

    const char file_name [] = "cthulhu.json";
    char path_to_read [PATH_MAX];
    snprintf(path_to_read, sizeof(path_to_read), "/lcm/cthulhu/bundles/%s/%s", ctr_id, file_name);

    //use json library to find value of imageLayoutVersion
    amxc_var_t* json_data = read_json(path_to_read);
    const char* bundle_version = GET_CHAR(json_data, "bundle_version");
    if (bundle_version) {
        ctr_info->bundle_version = strdup(bundle_version);
    }
    amxc_var_delete(&json_data);
    rc = 0;

exit:
    return rc;
}

static void iterate_containers(libcrun_container_list_t* list,
                              const char* state_root,
                              libcrun_error_t* crun_err,
                              amxc_var_t* ret) {
    int rc = -1;
    libcrun_container_list_t* iterator = NULL;

    amxc_var_init(ret);
    amxc_var_set_type(ret, AMXC_VAR_ID_LIST);

    for (iterator = list; iterator != NULL; iterator = iterator->next) {
        libcrun_container_status_t crun_container_status;
        cthulhu_ctr_info_t* ctr_info = NULL;

        if (iterator->name == NULL) {
            SET_ERR("Invalid container");
            continue;
        }

        const char* ctr_id = iterator->name;

        rc = libcrun_read_container_status (&crun_container_status, state_root, ctr_id, crun_err);
        if (rc < 0) {
            SET_ERR("Failed to read container status for container '%s', rc = %d", ctr_id, rc);
            continue;
        }

        rc = cthulhu_ctr_info_new(&ctr_info);
        if ((rc < 0) || (ctr_info == NULL)) {
            SET_ERR("Failed to create new ctr_info for container '%s', rc = %d", ctr_id, rc);
            continue;
        }

        ctr_info->ctr_id = strdup(ctr_id);
        int running = 0;
        int pid = crun_container_status.pid;
        const char* crun_container_status_string = NULL;

        rc = libcrun_get_container_state_string (ctr_id,
                                                 &crun_container_status,
                                                 state_root,
                                                 &crun_container_status_string,
                                                 &running,
                                                 crun_err);
        
        if ((rc < 0) || (crun_container_status_string == NULL)) {
            SET_ERR("Failed to get state string for container '%s', rc = %d", ctr_id, rc);
            continue;
        }

        if (!running) { pid = 0; }
        ctr_info->pid = pid;

        ctr_info->status = get_cthulhu_ctr_status(crun_container_status_string);
        if (ctr_info->status == cthulhu_ctr_status_unknown) {
            SET_ERR("Failed to map libcrun status '%s' onto cthulhu status for container '%s'",
                        crun_container_status_string, ctr_id);
        }

        if (crun_container_status.bundle) {
            SAH_TRACEZ_INFO(CTHULHU_CRUN, "Setting ctr_info for bundle '%s'", crun_container_status.bundle);
            ctr_info->bundle = strdup(crun_container_status.bundle);
            rc = set_bundle_version(ctr_id, ctr_info);
            if (rc != 0) {
                SET_ERR("Failed to set bundle version for container '%s'", ctr_id)
            }
        }

        //sb_id equivalent doesn't exist in crun so don't set

        if (crun_container_status.created != NULL) {
            ctr_info->created = strdup(crun_container_status.created);
        }

        if (crun_container_status.owner != NULL) {
            ctr_info->owner = strdup(crun_container_status.owner);
        }

        libcrun_free_container_status (&crun_container_status);
        amxc_var_add_new_cthulhu_ctr_info_t(ret, ctr_info);
    }
}

int cthulhu_crun_list(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    libcrun_context_t crun_ctx = {};
    libcrun_error_t crun_err;
    libcrun_container_list_t* list = NULL;
    const char* ctr_id = NULL;

    rc = init_libcrun_context(&crun_ctx, ctr_id, args, &crun_err);
    if (rc < 0) {
        SET_ERR("Failed to initialise libcrun context");
        goto exit;
    }

    rc = libcrun_get_containers_list (&list, crun_ctx.state_root, &crun_err);
    if (rc < 0) {
        SET_ERR("Failed to get container list");
        goto exit;
    }

    iterate_containers(list, crun_ctx.state_root, &crun_err, ret);
    rc = 0;

exit:
    libcrun_free_containers_list (list);
    destroy_libcrun_context(&crun_ctx);
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}
