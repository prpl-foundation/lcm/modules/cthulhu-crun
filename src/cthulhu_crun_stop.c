/****************************************************************************
**
** Copyright (c) 2023 Consult Red
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE 1 // to make strdup work

#include <string.h>
#include <pthread.h>
#include <time.h>

#include <amxc/amxc_lqueue.h>
#include <amxc/amxc_variant.h>

#include <cthulhu/cthulhu_defines.h>
#include <crun/container.h>
#include <crun/status.h>
#include <debug/sahtrace.h>

#include "cthulhu_crun_defines.h"
#include "cthulhu_crun_context.h"
#include "cthulhu_crun_console_socket.h"
#include "cthulhu_crun_stop.h"

#define WORKER_SLEEP_SECS 1
#define SIGNAL_ESCALATION_MS 5000

static const char primary_signal[] = "SIGTERM";
static const char secondary_signal[] = "SIGKILL";

static pthread_mutex_t worker_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t worker_cond = PTHREAD_COND_INITIALIZER;

typedef struct _worker {
    pthread_t thread;
    amxc_lqueue_t work;
    bool running;
} worker_t;

typedef struct _work_item {
    struct timespec start_ts;
    char* ctr_id;
    amxc_lqueue_it_t it;
} work_item_t;

static worker_t worker;

static int escalate_stop_signal(const char* ctr_id) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc;
    libcrun_context_t crun_ctx = {};
    libcrun_container_status_t ctr_status = {};
    libcrun_error_t crun_err;

    rc = init_libcrun_context(&crun_ctx, ctr_id, NULL, &crun_err);
    if (rc < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to initialise libcrun context for container '%s', rc = %d", ctr_id, rc);
        goto exit;
    }
    rc = libcrun_read_container_status (&ctr_status, crun_ctx.state_root, ctr_id, &crun_err);
    if (rc < 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to read container status for container '%s', rc = %d", ctr_id, rc);
        goto exit;
    }
    rc = libcrun_is_container_running(&ctr_status, &crun_err);
    if (rc != 0) {
        // Container is not in stopped state so escalate to secondary signal
        rc = libcrun_container_kill(&crun_ctx, ctr_id, secondary_signal, &crun_err);
        if (rc < 0) {
            SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to send %s to stop container '%s', rc = %d", secondary_signal, ctr_id, rc);
            goto exit;
        }
    }

exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

static void* container_stop_worker_thread(UNUSED void* arg) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    do {
        pthread_mutex_lock(&worker_mutex);
        if (amxc_lqueue_is_empty(&worker.work)) {
            // Block until condition is signalled
            pthread_cond_wait(&worker_cond, &worker_mutex);
        }
        pthread_mutex_unlock(&worker_mutex);

        // Process work queue until empty
        while (worker.running) {
            pthread_mutex_lock(&worker_mutex);
            amxc_lqueue_it_t* work_it = amxc_lqueue_remove(&worker.work);
            pthread_mutex_unlock(&worker_mutex);
            if (work_it == NULL) {
                // No more queued work
                break;
            }
            work_item_t* work_item = amxc_llist_it_get_data(work_it, work_item_t, it);
            float elapsed_ms = 0.0f;
            struct timespec current_ts;
            // Wait for timeout before checking whether to escalate stop signal
            while (worker.running) {
                if (clock_gettime(CLOCK_MONOTONIC, &current_ts) != 0) {
                   SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to get clock time in worker thread"); 
                   elapsed_ms = SIGNAL_ESCALATION_MS;
                } else {
                    elapsed_ms = ((current_ts.tv_sec - work_item->start_ts.tv_sec) * 1000.0f) +
                                 ((current_ts.tv_nsec - work_item->start_ts.tv_nsec) / 1000.f);
                }
                if (elapsed_ms >= (float)SIGNAL_ESCALATION_MS) {
                    escalate_stop_signal(work_item->ctr_id);
                    free(work_item->ctr_id);
                    free(work_item);
                    break;
                }
                sleep(WORKER_SLEEP_SECS);
            }
        }
    } while (worker.running);

    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s exited", __func__);
    return NULL;
}

static int add_work_item(const char* ctr_id) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    work_item_t* work_item = (work_item_t*) calloc(1, sizeof(work_item_t));
    if (work_item == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to allocate memory for new work item");
        goto exit;
    }

    work_item->ctr_id = strdup(ctr_id);
    if (work_item->ctr_id == NULL) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to duplicate ctr_id");
        goto exit;
    }

    rc = clock_gettime(CLOCK_MONOTONIC, &work_item->start_ts);
    if (rc != 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to get clock time");
        goto exit;
    }

    pthread_mutex_lock(&worker_mutex);
    rc = amxc_lqueue_add(&worker.work, &work_item->it);
    pthread_cond_signal(&worker_cond);
    pthread_mutex_unlock(&worker_mutex);

exit:
    if ((rc < 0) && (work_item != NULL)) {
        free(work_item->ctr_id);
        free(work_item);
    }
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

int cthulhu_crun_stop(UNUSED const char* function_name, amxc_var_t* args, amxc_var_t* ret) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc = -1;
    libcrun_context_t crun_ctx = {};
    libcrun_error_t crun_err;
    char socket_path [MAX_SOCKET_PATH_LENGTH];

    const char* ctr_id = GET_CHAR(args, CTHULHU_CONTAINER_ID);
    if (string_is_empty(ctr_id)) {
        SET_ERR("ContainerId not provided in args");
        goto exit;
    }

    int local_ret_val = init_libcrun_context(&crun_ctx, ctr_id, args, &crun_err);
    if (local_ret_val < 0) {
        SET_ERR("Failed to initialise libcrun context for container '%s', rc = %d", ctr_id, local_ret_val);
        goto exit;
    }

    if (snprintf(socket_path, sizeof(socket_path), "/tmp/%s.sock", ctr_id) < 0)
    {
        SET_ERR("Console socket path truncated for container '%s'", ctr_id);
    }
    close_console_socket(socket_path);

    local_ret_val = libcrun_container_kill(&crun_ctx, ctr_id, primary_signal, &crun_err);
    if (local_ret_val < 0) {
        SET_ERR("Failed to send %s to stop container '%s', rc = %d", primary_signal, ctr_id, local_ret_val);
        goto exit;
    }

    rc = 0; // Successfully issued primary stop signal

    if (add_work_item(ctr_id) < 0) {
        SET_ERR("Failed to add work item so sending %s to stop container '%s', rc = %d", secondary_signal, ctr_id, local_ret_val);
        local_ret_val = libcrun_container_kill(&crun_ctx, ctr_id, secondary_signal, &crun_err);
        if (local_ret_val < 0) {
            SET_ERR("Failed to send %s to stop container '%s', rc = %d", secondary_signal, ctr_id, local_ret_val);
            goto exit;
        }
    }

exit:
    destroy_libcrun_context(&crun_ctx);
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

int cthulhu_crun_stop_init(void) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);

    int rc;
    worker.running = true;
    rc = amxc_lqueue_init(&worker.work);
    if(rc != 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to initialize worker queue, rc = %d", rc);
        goto exit;
    }
    rc = pthread_create(&worker.thread, NULL, container_stop_worker_thread, NULL);
    if (rc != 0) {
        SAH_TRACEZ_ERROR(CTHULHU_CRUN, "Failed to create worker thread, rc = %d", rc);
        goto exit;
    }
exit:
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s returned %d", __func__, rc);
    return rc;
}

void cthulhu_crun_stop_destroy(void) {
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s entered", __func__);
    worker.running = false;
    pthread_cond_signal(&worker_cond);
    pthread_join(worker.thread, NULL);
    pthread_cond_destroy(&worker_cond);
    pthread_mutex_destroy(&worker_mutex);
    amxc_lqueue_clean(&worker.work, NULL);
    SAH_TRACEZ_INFO(CTHULHU_CRUN, "%s exited", __func__);
}