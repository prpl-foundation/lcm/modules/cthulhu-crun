# Cthulhu-crun module

[[_TOC_]]

## Introduction

This module provides a backend for [cthulhu](https://gitlab.com/prpl-foundation/lcm/modules/cthulhu) that allows one to create and control crun containers.

## Building and installing

### Docker container

You could install all tools needed for testing and developing on your local machine, but it is easier to just use a pre-configured environment. Such an environment is already prepared for you as a docker container.

1. Install docker

    Docker must be installed on your system.

    [Get Docker Engine for appropriate platform]( https://docs.docker.com/engine/install/)

    Make sure you user id is added to the docker group:

    ```bash
    sudo usermod -aG docker $USER
    ```

2. Fetch the container image

    To get access to the pre-configured environment, all you need to do is pull the image and launch a container.

    Pull the image:

    ```bash
    docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    Before launching the container, you should create a directory which will be shared between your local machine and the container.

    ```bash
    mkdir -p ~/workspace/lcm
    ```

3. Launch the container:

    ```bash
    docker run -ti -d --name oss-dbg --restart always --cap-add=SYS_PTRACE --sysctl net.ipv6.conf.all.disable_ipv6=1 -e "USER=$USER" -e "UID=$(id -u)" -e "GID=$(id -g)" -v ~/amx/:/home/$USER/workspace/lcm/ registry.gitlab.com/soft.at.home/docker/oss-dbg:latest
    ```

    The `-v` option bind mounts the local directory for the lcm project in the container, at the exact same place.
    The `-e` options create environment variables in the container. These variables are used to create a user name with exactly the same user id and group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

    ```bash
    docker exec -ti --user $USER oss-dbg /bin/bash
    ```

### Building

#### Prerequisites

- [lib\_amxc](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc)
- [lib\_amxj](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj)
- [lib\_amxm](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxm)
- [lib\_amxp](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp)
- [lib\_amxd](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd)
- [lib\_amxo](https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo)
- [libcthulhu](https://gitlab.com/prpl-foundation/lcm/libraries/libcthulhu)
- [lib\_sahtrace](https://gitlab.com/soft.at.home/network/libsahtrace)
- [yajl](https://lloyd.github.io/yajl)
- [crun](https://github.com/containers/crun)

#### Build cthulhu-crun

1. Clone the git repository

    To be able to build it, you need the source code. So make a directory for the LCM and clone this library in it.

    ```bash
    mkdir -p ~/workspace/lcm/modules
    cd ~/workspace/lcm/modules
    git clone https://gitlab.com/prpl-foundation/lcm/modules/cthulhu-crun
    ```

2. Apply patch to crun, build and install

   The cthulhu-crun module depends on a specific version of crun with a patch applied to expose the version number.
   When building crun note that the --enabled-shared flag passed to ./configure ensures that libcrun is built as
   a shared library rather than being statically linked into crun.
   We must also install the libocispec that comes with crun so that cthulhu-crun can use the same version.

   ```bash
   mkdir ~/crun
   cd ~/crun
   git clone -b 1.6 --depth 1 https://github.com/containers/crun.git .

   patch -p1 < ~/workspace/lcm/modules/cthulhu-crun/patches/crun/0001-Add-libcrun-get-version.patch

   ./autogen.sh
   ./configure --enable-shared
   sudo make install -j$(nproc)
   sudo ln -f -s /usr/local/bin/crun /usr/bin/crun

   sudo mkdir -p /usr/local/include/crun
   sudo cp ./config.h /usr/local/include/crun
   sudo cp ./src/libcrun/container.h /usr/local/include/crun
   sudo cp ./src/libcrun/error.h /usr/local/include/crun
   sudo cp ./src/libcrun/status.h /usr/local/include/crun

   sudo mkdir -p /usr/local/include/crun/libocispec
   sudo cp ./libocispec/src/*.h /usr/local/include/crun/libocispec

   sudo mkdir -p /usr/local/lib
   sudo cp ./libocispec/.libs/libocispec.* /usr/local/lib

   sudo ldconfig

3. Build it

    ```bash
    cd ~/workspace/lcm/modules/cthulhu-crun
    make
   ```

### Installing

#### Using make target install

You can install your own compiled version easily by running the install target.

```bash
cd ~/workspace/lcm/modules/cthulhu-crun
sudo -E make install
```

#### Using package

From within the container you can create packages.

```bash
cd ~/workspace/lcm/modules/cthulhu-crun
make package
```

The packages generated are:

```
~/workspace/lcm/applications/modules/cthulhu-crun/cthulhu-crun-<VERSION>.tar.gz
```

You can copy these packages and extract/install them.
